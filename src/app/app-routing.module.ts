import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/authguard.service';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'dashboard', canActivate: [AuthGuard], loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule' },
  { path: 'qr-details', canActivate: [AuthGuard], loadChildren: './pages/qr-details/qr-details.module#QrDetailsPageModule' },
 // { path: 'ckeck-out', canActivate: [AuthGuard], loadChildren: './pages/ckeck-out/ckeck-out.module#CkeckOutPageModule' },
  { path: 'booking-detail', loadChildren: './pages/dashboard/bookings/booking-detail/booking-detail.module#BookingDetailPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
