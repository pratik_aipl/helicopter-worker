import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ThankModalComponent } from '../dashboard/modal/thank-modal/thank-modal.component';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qr-details',
  templateUrl: './qr-details.page.html',
  styleUrls: ['./qr-details.page.scss'],
})
export class QrDetailsPage implements OnInit {

  compareWith: any;
  selectType: any;
  scanData: any;
  scData: any;
  helicopter = [];
  routes = [];
  user: any = {};
  isPilot:any;
  route_id:any;
  assignForm: FormGroup;
  constructor(public modalController: ModalController, public commonService: CommonService,
    public tools: Tools, public formBuilder: FormBuilder, public route: Router) {
    this.scData = this.commonService.getScanData()
    this.user = this.commonService.getUserData();
    this.assignForm = this.formBuilder.group({
      helicopter: ['', Validators.required],
      route: [''],
    });
    console.log('ionViewDidEnter --2 ',this.user.user.todaySchedule);
    if(this.scData !=undefined)
    this.getData();
  }

  ngOnInit() {
    
  }
  ionViewDidEnter(){
   
  }
  getData() {
    if(this.user.user.todaySchedule){
      // this.commonService.getRouteList(this.scData.user_id,this.user.user.routeData.RouteDetailID).subscribe(response => {
        this.tools.openLoader();
      this.commonService.getRouteList(this.scData.user_id,this.user.user.todaySchedule.HelipadID).subscribe(response => {
          this.tools.closeLoader();
          let res: any = response;
          this.scanData = res;
          this.isPilot=this.scanData.customer_details.Type=='C'?false:true;
    
          if(this.isPilot){
            this.route_id=this.scanData.route.RouteID;
          }
                   // console.log(this.scanData.route_detail_list);
        }, (error: Response) => {
          this.tools.closeLoader();
          let err:any = error;
          console.log('Route err ',err);
          if (err) {
            this.tools.openAlert(err.message);
          }
        });
      }else{
        this.tools.openAlert('Todays Not Assign Any Helipad!');
      }
    
  }
  AssignClick() {
    let postData = new FormData();
    postData.append("PassID", this.isPilot?'':this.scData.pass_id,);
    postData.append("HelicopterID", this.assignForm.get('helicopter').value );
    postData.append("RouteID",  this.isPilot?this.scanData.route.RouteID:'');
    postData.append("RouteDetailID", this.isPilot?this.user.user.todaySchedule.HelipadID:this.assignForm.get('route').value);
    postData.append("CustomerOrPilot",  this.scData.user_id);
    postData.append("Type", this.scanData.customer_details.Type);
    postData.append("LuggageQRCodeID", this.scData.LuggageQRCodeID);

    console.log('Params --> ', postData);
    this.tools.openLoader();
    this.commonService.AssignRide(postData).subscribe(response => {
      this.tools.closeLoader();
      console.log('response --> ', response);
      this.showModel();
    }, (error: Response) => {
      this.tools.closeLoader();
      console.log(error);
      if (error) {
        let err:any = error;
        // this.tools.openAlert(JSON.parse(error.error).message);
        this.tools.openAlertToken(err.status, err.error.message);
      }
    });
  }
  async showModel() {
    const modal = await this.modalController.create({
      component: ThankModalComponent,
      cssClass: 'thank-modal',
      componentProps: { value: "sample" }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then((data) => {
        console.log(data);
        this.route.navigateByUrl('/');
      });
  }
}
