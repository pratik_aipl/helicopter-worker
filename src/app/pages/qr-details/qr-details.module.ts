import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { QrDetailsPage } from './qr-details.page';
import { ThankModalComponent } from '../dashboard/modal/thank-modal/thank-modal.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { StarRatingModule } from 'angular-star-rating';
const routes: Routes = [
  {
    path: '',
    component: QrDetailsPage
  },{
    path: 'thank-model',
    component: ThankModalComponent
  },
];

@NgModule({
  entryComponents: [ThankModalComponent],
  imports: [
    SharedModule,
    StarRatingModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [QrDetailsPage, ThankModalComponent]
})
export class QrDetailsPageModule {}
