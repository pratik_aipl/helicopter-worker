import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CkeckOutPage } from './ckeck-out.page';

describe('CkeckOutPage', () => {
  let component: CkeckOutPage;
  let fixture: ComponentFixture<CkeckOutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CkeckOutPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CkeckOutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
