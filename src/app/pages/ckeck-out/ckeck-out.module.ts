import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarRatingModule } from 'angular-star-rating';
import { CkeckOutPage } from './ckeck-out.page';

import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: CkeckOutPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    StarRatingModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [CkeckOutPage]
})
export class CkeckOutPageModule {}
