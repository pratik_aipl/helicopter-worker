import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilePage } from './profile.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExpandlistComponent } from 'src/app/widgets/expandlist/expandlist.component';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  }
];

@NgModule({
  imports: [
    SharedModule,
    // StarRatingModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [ProfilePage,ExpandlistComponent]
})
export class ProfilePageModule {}
