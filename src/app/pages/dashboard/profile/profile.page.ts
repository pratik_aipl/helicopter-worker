import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: any = {};
  constructor(public router: Router,public commonService: CommonService,
    public tools: Tools) {
    this.user = this.commonService.getUserData();
  }

  ngOnInit() {

  }
  ionViewDidEnter(){
    this.getUserData();
  }
  getUserData(){
    this.tools.openLoader();
    this.commonService.getUserInfo().subscribe(response => {
      this.tools.closeLoader();     
      let res: any = response;
      console.log('Profile ',res);
      this.commonService.setUserData(res.data,this.commonService.getLoginToken());
      this.user = res.data;
      // this.router.navigateByUrl('/dashboard');
    }, (error: Response) => {
      this.tools.closeLoader();
      let err:any = error;
      console.log(err);
      if (err) {
        this.tools.openAlertToken(err.status,err.error.message);
      }
    });
  }
  public captureName(event: any) : void
  {
     console.log(`Captured name by event value: ${event}`);
  }
}
