import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  isShow=false;
  @ViewChild('myTabs') tabs: IonTabs;
  tabName: any = 'Sacn';
  constructor(public tools:Tools) { }
  ngOnInit() {
  }

  getSelectedTab(): void {
    if( this.tabs.getSelected() == 'profile'){
      this.isShow=true;
      this.tabName = 'Profile';
    }else if( this.tabs.getSelected() == 'scan'){
      this.isShow=false;
      this.tabName = 'Scan';
    }else{
      this.isShow=false;
      this.tabName = 'Bookings';
    }
    console.log(this.tabs.getSelected())
  }
  logout() {
    this.tools.presentLogout('Are you sure you want to logout?','Logout','Cancel')
  }
}
