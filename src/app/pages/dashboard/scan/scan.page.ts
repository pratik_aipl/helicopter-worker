import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit, OnDestroy  {

  scannedResult: any;
  user: any = {};

  readyToScan: boolean = false;
  scanning: boolean = false;
  intervalHandle: any = null;

  constructor(public router: Router, private qrScanner: QRScanner, public tools: Tools, public commonServices: CommonService) {
    this.user.user = commonServices.getUserData();
  }

  ngOnDestroy(): void {
    if(this.intervalHandle !=null)
    clearInterval(this.intervalHandle);
      this.intervalHandle = null;
  }
  ionViewDidEnter(){
    console.log('ionViewDidEnter');
    if (this.intervalHandle === null) {
      this.intervalHandle = setInterval(() => {
        this.getUserData();
      }, 7000);
    } else {
      clearInterval(this.intervalHandle);
      this.intervalHandle = null;
    }
  }
  ionViewDidLeave(){
    console.log('ionViewDidLeave');
    if(this.intervalHandle !=null)
    clearInterval(this.intervalHandle);
      this.intervalHandle = null;
  }
  ngOnInit() {
  }
  Scan() {
    if (this.user.user.todaySchedule) {
      // Optionally request the permission early
      this.qrScanner.prepare()
        .then((status: QRScannerStatus) => {
          if (status.authorized) {
            // camera permission was granted
          this.scanning = true;
            this.startScanning();           
            this.qrScanner.show();
            // start scanning
            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
              console.log('Scanned something', text);
              this.scanning = false;
              this.closeScanner();
              this.stopScanning();
              scanSub.unsubscribe(); // stop scanning
              this.scannedResult = text;
              this.commonServices.saveScanData(text);
              this.router.navigateByUrl('/qr-details');
            });
            this.qrScanner.show();
          } else if (status.denied) {
            console.log('Permission Denied');
            this.qrScanner.openSettings();
            // camera permission was permanently denied
            // you must use QRScanner.openSettings() method to guide the user to the settings page
            // then they can grant the permission from there
          } else {
            // permission was denied, but not permanently. You can ask for permission again at a later time.
          }
        })
        .catch((e: any) => console.log('Error is', e));


    } else {
      this.tools.openAlert('You are not scheduled today. Please contact Administrator.');
    }


  }

  startScanning(): void {
    console.log("Started Scanning");
    (window.document.querySelector('html') as HTMLElement).classList.add('cameraView');
  }

  stopScanning(): void {
    console.log("Stopped Scanning");
    (window.document.querySelector('html') as HTMLElement).classList.remove('cameraView');
  }

  closeScanner() {
    this.scanning = false;
    this.qrScanner.hide();
    this.qrScanner.destroy();
  }
  ionViewWillEnter() {
    this.getUserData();
  }
  getUserData() {
    this.commonServices.getUserInfo().subscribe(response => {
      let res: any = response;
      console.log(res);
      this.commonServices.setUserData(res.data, this.commonServices.getLoginToken());
      this.user = this.commonServices.getUserData();
    }, (error: Response) => {
      console.log(error);
      let err:any = error;
      if (err) {
        this.tools.openAlertToken(err.status, err.message);
      }
    });
  }
}
