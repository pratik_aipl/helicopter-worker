import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: "tabs",
    component: DashboardPage,
    children: [
      {
        path: "",
        redirectTo: "tabs/(scan:scan)",
        pathMatch: "full"
      },
      {
        path: "scan",
        children: [
          {
            path: "",
            loadChildren: "../dashboard/scan/scan.module#ScanPageModule"
          }
        ]
      },
      {
        path: "bookings",
        children: [
          {
            path: "",
            loadChildren:"../dashboard/bookings/bookings.module#BookingsPageModule"
          }
        ]
      },
      {
        path: "profile",
        children: [
          {
            path: "",
            loadChildren:"../dashboard/profile/profile.module#ProfilePageModule"
          }
        ]
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/scan",
    pathMatch: "full"
  },
  
];


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
