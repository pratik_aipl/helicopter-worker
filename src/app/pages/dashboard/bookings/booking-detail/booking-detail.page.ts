import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-booking-detail',
  templateUrl: './booking-detail.page.html',
  styleUrls: ['./booking-detail.page.scss'],
})
export class BookingDetailPage implements OnInit {

  bookRide:any={}
  constructor(public commonServices:CommonService) { 
    this.bookRide = this.commonServices.getBookedItem();
    }

  ngOnInit() {
  }

}
