import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { BookingsPage } from './bookings.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: BookingsPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BookingsPage]
})
export class BookingsPageModule {}
