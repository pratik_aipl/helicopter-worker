import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tools } from 'src/app/tools';
import { CommonService } from 'src/app/shared/common.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit {
  bookingList = [];
  constructor(public router: Router,public tools:Tools,public commonService:CommonService) { 

  }
  ngOnInit() {
  }
  ionViewDidEnter(){
   
    this.getBooklist();
  }
  getBooklist() {
    this.tools.openLoader();
    this.commonService.getBookingList().subscribe(response => {
      this.tools.closeLoader();
      let res: any = response;
      console.log('ionViewDidEnter --2 ',res.data);
      this.bookingList=res.data;
    }, (error: Response) => {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Api Error Data ',err);      
      if (err) {
        this.tools.openAlertToken(err.status,err.error.message);
      }
    });
  }
  bookClick(item){
   // console.log(item);
   this.commonService.setBookedItem(item);
   this.router.navigateByUrl('/booking-detail');
  }
}
