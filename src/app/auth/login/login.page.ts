import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/shared/common.service';
import { Tools } from 'src/app/tools';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  
  loginForm: FormGroup;
  constructor(public router: Router,
    public formBuilder: FormBuilder,
    public commonService: CommonService,
    public tools: Tools) { 
      this.tools.callOneSignal();
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
      });
    }

  ngOnInit() {
  }

  onSubmit() {
    this.tools.openLoader();
    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value;
    this.commonService.login(email, password).subscribe(response => {
      this.tools.closeLoader();     
      let res: any = response;
      console.log('Login Response ',res.login_token);
      this.loginForm.reset();
      this.commonService.setUserData(res.data,res.login_token);
      this.router.navigateByUrl('/dashboard');
    }, (error: Response) => {
      this.tools.closeLoader();
      let err:any = error;
      console.log('Login error ',err);
      if (err) {
           this.tools.closeLoader();
      this.tools.openAlertToken(err.status, err.error.message);
        // this.tools.openAlert(JSON.parse(error.error).message);
      }
    });
  }
}
