import { Component, ViewChildren, QueryList } from '@angular/core';

import { Platform, ModalController, IonRouterOutlet, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,private oneSignal: OneSignal,
    private router: Router,
    private toast: ToastController,
    public modalCtrl: ModalController,
  ) {
    this.initializeApp();
    this.backButtonEvent();
  }
  callOneSignal() {
    this.oneSignal.startInit('a79b3a06-cf3c-4267-88d7-44377ba9ad3f', '546015657170');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    
    
    this.oneSignal.handleNotificationReceived().subscribe(() => {
     // do something when notification is received
    });
    
    this.oneSignal.handleNotificationOpened().subscribe(() => {
      // do something when a notification is opened
    });
    
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((id) => {
      console.log('userId ==> ',id.userId);
      console.log('pushToken ==> ',id.pushToken);
      localStorage.setItem('PlearID',id.userId);
      
    });
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.overlaysWebView(true);
      this.statusBar.overlaysWebView(true);
      // this.statusBar.backgroundColorByHexString('#ffffff');
      this.statusBar.show();
      // this.statusBar.backgroundColorByHexString('#002e5b');    
      this.splashScreen.hide();
    });
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(999999, () => {
      console.log('subscribeWithPriority Route URL ', this.router.url);
      // navigator['app'].exitApp();
      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        if (this.router.url === '/dashboard/tabs/scan') {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp(); // work in ionic 4
          } else {
            this.presentToast('Press back again to exit App.');
            this.lastTimeBackPress = new Date().getTime();
          }
        } else if (this.router.url === '/dashboard/tabs/bookings') {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp(); // work in ionic 4
          } else {
            this.presentToast('Press back again to exit App.');
            this.lastTimeBackPress = new Date().getTime();
          }
        } else if (this.router.url === '/dashboard/tabs/profile') {
          if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
            navigator['app'].exitApp(); // work in ionic 4
          } else {
            this.presentToast('Press back again to exit App.');
            this.lastTimeBackPress = new Date().getTime();
          }
        } else if (this.router.url === '/login') {
          navigator['app'].exitApp(); // work in ionic 4
        }
        else if (outlet && outlet.canGoBack()) {
          outlet.pop();
        }
      });
    });
    this.platform.backButton.subscribe(async () => {
      console.log('subscribe Route ', this.router.url);
    });
  }

  async presentToast(msg) {
    const toast = await this.toast.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
