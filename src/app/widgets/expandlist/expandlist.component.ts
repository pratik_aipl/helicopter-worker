import { CommonService } from 'src/app/shared/common.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-expandlist',
  templateUrl: './expandlist.component.html',
  styleUrls: ['./expandlist.component.scss'],
})
export class ExpandlistComponent implements OnInit {

  /**
   * The name of the technology that will be displayed as the title for the accordion header
   * @public
   * @property name
   * @type {string}
   */
  @Input()
  name : string;
  @Input()
  isShow : string;


   /**
   * The sublist of the schedule that will be displayed within the accordion body (when activated
   * by the schedule)
   * @public
   * @property description
   * @type {string}
   */
  @Input()
  scheduleData:any[];


  /**
   * The official logo identifying the technology that will be displayed within the accordion body (when activated
   * by the user)
   * @public
   * @property image
   * @type {string}
   */
  @Input()
  image : string;


  /**
   * The change event that will be broadcast to the parent component when the user interacts with the component's
   * <ion-button> element
   * @public
   * @property change
   * @type {EventEmitter}
   */
  @Output()
  change : EventEmitter<string> = new EventEmitter<string>();


  /**
   * Determines and stores the accordion state (I.e. opened or closed)
   * @public
   * @property isMenuOpen
   * @type {boolean}
   */
  public isMenuOpen : boolean = false;



  constructor(private commonService:CommonService) {

  }



  ngOnInit() {
  }



  /**
   * Allows the accordion state to be toggled (I.e. opened/closed)
   * @public
   * @method toggleAccordion
   * @returns {none}
   */
  public toggleAccordion(name,scheduleData) : void
  {
    console.log('toggleAccordion ',this.isMenuOpen);
    console.log('toggleAccordion name ',name);
    console.log('toggleAccordion scheduleData ',scheduleData);
    if(this.isMenuOpen == false){   
      var scId='';
      // this.routeUpdateStatus(Array.prototype.map.call(this.scheduleData, function(item) {            
      //   if(item.isShow==0){
      //   return item.ScheduleID;           
      //   }
      //   return -1; 
      // }).join(","));

      for (let i = 0; i < this.scheduleData.length; i++) {
        if(this.scheduleData[i].isShow == "0"){
          scId = scId + this.scheduleData[i].ScheduleID+',';
          }
        }
        console.log('toggleAccordion scId ',scId);
        this.routeUpdateStatus(scId.substring(0, scId.length-1))
    }else{
      this.isShow="0";
      for (let i = 0; i < this.scheduleData.length; i++) {
        this.scheduleData[i].isShow = "1";        
      }
    }
    this.isMenuOpen = !this.isMenuOpen;
  }
routeUpdateStatus(date){
  console.log('scheduleStatusUpdate --> call params ',date);
  if(date != ''){
    this.commonService.scheduleStatusUpdate(date).subscribe(response => {
      let res: any = response;
      console.log('scheduleStatusUpdate --> call response ',res);      
    }, (error: Response) => {
      let err:any = error;
      console.log(err);
    });
  }
  }

  /**
   * Allows the value for the <ion-button> element to be broadcast to the parent component
   * @public
   * @method broadcastName
   * @returns {none}
   */
  public broadcastName(name : string) : void
  {
     this.change.emit(name);
  }

}
