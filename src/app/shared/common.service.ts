import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';
import { Tools } from '../tools';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  deviceInfo;
  bacisAuth; 
  // options;
  httpOptions: any;
  constructor(public auth: AuthGuard,public http: HttpClient,public tool:Tools) {
    this.deviceInfo = this.getDeviceInfo();
    this.bacisAuth = 'Basic ' + btoa(environment.username + ":" + environment.password);
  }
 
  login(email, password): any {

    var dID;
    if (!localStorage.getItem('PlearID')) {
      dID = localStorage.getItem('PlearID') == '' ? "111111" : localStorage.getItem('PlearID');
    } else {
      dID = '111111'
    }
    let postData = new FormData();
    postData.append("EmailID", email);
    postData.append("Password", password);
    postData.append("DeviceID", dID);
    var httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Authorization': this.bacisAuth,
        'X-CI-HELICOPTER-API-KEY': environment.apikey,
      })
    }
    return this.http.post(environment.BaseUrl + 'login',postData, httpOptions);
  }
  AssignRide(data): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': this.bacisAuth,
            'X-CI-HELICOPTER-API-KEY': environment.apikey,
            'User-Id': this.getUserId(),
            'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
      })
    }
    return this.http.post(environment.BaseUrl + 'AssignRide', data, httpOptions);
  }
  getUserInfo(): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': this.bacisAuth,
            'X-CI-HELICOPTER-API-KEY': environment.apikey,
            'User-Id': this.getUserId(),
            'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
      })
    }
    return this.http.post(environment.BaseUrl + 'UserInfo', {}, httpOptions);
  }
  scheduleStatusUpdate(schID): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': this.bacisAuth,
            'X-CI-HELICOPTER-API-KEY': environment.apikey,
            'User-Id': this.getUserId(),
            'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
      })
    }
     let postData = new FormData();
    postData.append("ScheduleID", schID);
    return this.http.post(environment.BaseUrl + 'UserInfo/update_status',postData, httpOptions);
  }
  getRouteList(user_id,RouteDetailID): any {
    var httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': this.bacisAuth,
            'X-CI-HELICOPTER-API-KEY': environment.apikey,
            'User-Id': this.getUserId(),
            'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
      })
    }
    let postData = new FormData();
    postData.append("user_id", user_id);
    postData.append("RouteDetailID", RouteDetailID);
    return this.http.post(environment.BaseUrl + 'RouteList', postData, httpOptions);
  }
  getBookingList(): any {
  var httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Authorization': this.bacisAuth,
          'X-CI-HELICOPTER-API-KEY': environment.apikey,
          'User-Id': this.getUserId(),
          'X-CI-HELICOPTER-LOGIN-TOKEN': this.getLoginToken(),
    })
  }
    return this.http.get(environment.BaseUrl + 'BookingList',  httpOptions);
  }

  getCurrentLatLng() {
    navigator.geolocation.getCurrentPosition((res) => {
      let currentLatLng: any = {};
      currentLatLng.latitude = res.coords.latitude;
      currentLatLng.longitude = res.coords.longitude;
      return currentLatLng;
    });
  }

  // GET & SET USER DATA
  saveScanData(scanData) {
    console.log(scanData)
    localStorage.setItem('scanData', scanData);
  }
  getScanData() {
    if (localStorage['scanData']) {
      return JSON.parse(localStorage['scanData']);
    }
    return;
  }
  setUserData(userData,login_token) {
    console.log('Login User ',userData)

    localStorage.setItem('user_data', JSON.stringify(userData));
    localStorage.setItem('login_token',login_token);
    localStorage.setItem('user_id', userData.user.id);
    console.log('Login User ',localStorage.getItem('user_id'))
  }
  setSelectedCountry(item) {
   localStorage.setItem('sel_route', JSON.stringify(item));
  }
  
  getSelectedCountry() {
    if (localStorage['sel_route']) {
      return JSON.parse(localStorage['sel_route']);
    }
    return;
  }
  setSelectedRoute(item) {
    localStorage.setItem('sel_route_sub', JSON.stringify(item));
  }
  getSelectedRoute() {
    if (localStorage['sel_route_sub']) {
      return JSON.parse(localStorage['sel_route_sub']);
    }
    return;
  }
  setBookedItem(item) {
    if(item != null)
    localStorage.setItem('selBooking', JSON.stringify(item));
    else
    localStorage.setItem('selBooking','');
  }
  getBookedItem() {
    if (localStorage['selBooking']) {
      return JSON.parse(localStorage['selBooking']);
    }
    return;
  }
  getUserData() {
    if (localStorage['user_data']) {
      return JSON.parse(localStorage['user_data']);
    }
    return;
  }
  getUserId() {
    if (localStorage['user_id']) {
      return localStorage['user_id'];
    }
    return;
  }

  getLoginToken() {
    if (localStorage['login_token']) {
      return localStorage['login_token'];
    }
    return;
  }


  // GET & SET DEVICE INFO
  setDeviceInfo(deviceInfo) {
    localStorage.setItem('deviceInfo', JSON.stringify(deviceInfo));
  }

  getDeviceInfo() {
    if (localStorage['deviceInfo']) {
      return JSON.parse(localStorage['deviceInfo']);
    }
    return;
  }
}
